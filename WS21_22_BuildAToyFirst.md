# Build a toy (first) - Further Reading

Winter Semester 2021 / 2022  
[Game Design // UE](http://gamedesign.ue-germany.de/)  
[Prof. Csongor Baranyai](http://www.csongorb.com)

## Module Essentials

* https://itch.io/jam/ws2122-build-a-toy-first
* https://bitbucket.org/btkgamedesign/workspace/projects/WS2122_TOYFIRST
* Miro > Uhrwerk Orange > WS21/22 - Build a toy (first)
* Teams > WS21/22 - Build a toy (first) > Appendix > Files (!)
* Repository Structure
	* https://bitbucket.org/btkgamedesign/creditsthegame  
	* [https://keepachangelog.com/](https://keepachangelog.com/)
	* [https://www.gamesasresearch.com/](https://www.gamesasresearch.com/) (MDMA)

## Project Development

### Design Notes / Journals

* Jordan Mechner Journals
	* https://jordanmechner.com/store/the-making-of-karateka/
	* https://jordanmechner.com/store/the-making-of-prince-of-persia/
	* https://jordanmechner.com/backstage/journals/
* https://github.com/oliverbenns/john-carmack-plan
* https://www.gamesasresearch.com/examples

### Game Development as Play

* Searching the Game
	* [Exploring Sim City: A Conscious Process of Discovery (Dan Moskowitz)](https://www.youtube.com/watch?v=eZfj7LEFT98)
	* [How Jonathan Blow Designs a Puzzle (Game Maker's Toolkit)](https://www.youtube.com/watch?v=2zK8ItePe3Y)
* Build the toy first
	* Kyle Gabler
		* [2009 Global Game Jam Keynote (starts at 5:40)](https://www.youtube.com/watch?v=aW6vgW8wc6c)
		* https://www.gamasutra.com/view/feature/130848/how_to_prototype_a_game_in_under_7_.php
	* [Jesse Schell: The Art of Game Design - A Book of Lenses](https://www.schellgames.com/art-of-game-design/)
		* #5: The Lens of Fun
		* #17: The Lens of The Toy
		* #100: The Lens of Love
* Game Development Process
	* https://en.wikipedia.org/wiki/Cone_of_Uncertainty
	* [http://www.devolution.online](http://www.devolution.online)
* Games as Experiments (what if?)
	* [http://www.experimental-gameplay.org/](http://www.experimental-gameplay.org/)
	* https://what-if.xkcd.com/archive/
	* http://gamephilosophy.org/pcg2016/site/assets/files/1015/moring_-_experimental_systems.pdf
	* [https://www.gamesasresearch.com/](https://www.gamesasresearch.com/)
* Tools
	* http://leveldesignporn.csongorb.com/tooldesign.html
	* http://leveldesignporn.csongorb.com/anyleveleditor.html

## Play & Playfulness

* Experimentality
	* https://www.aplayfulpath.com/experimentality/
* Play
	* http://www.deepfun.com/articles/
	* https://en.wikipedia.org/wiki/Play_(activity)
	* [Documentary: The Promise of Play (YouTube)](https://www.youtube.com/playlist?list=PLA66825B802731F20)
	* https://tocaboca.com/magazine/
	* [http://www.journalofplay.org/](http://www.journalofplay.org/)
* Bernie DeKoven
	* https://www.deepfun.com/bernie-dekoven-fun-theorist/
	* [https://www.deepfun.com/](https://www.deepfun.com/)
	* [https://www.aplayfulpath.com](https://www.aplayfulpath.com)
	* https://www.psychologytoday.com/gb/blog/having-fun
* Toys and Toy Design
	* https://www.wired.com/2011/01/the-5-best-toys-of-all-time/
	* https://museen.nuernberg.de/fileadmin/mdsn/pdf/Spielzeugmuseum/Downloads/Literaturliste_Spielzeug.pdf
	* Smarty Pants: Play & Playthings Report
		* [Play & Playthings Report 2018 - Excerpt](https://assets.website-files.com/5435eb4d1e426bb420ac990f/5c0e9721b6a5e9175bd940be_2018%20Play%20%26%20Playthings%20Report%20from%20Smarty%20Pants%20EXCERPT.pdf)
		* [Play & Playthings Report 2017 - Excerpt](https://uploads-ssl.webflow.com/5435eb4d1e426bb420ac990f/5a3170d30672a700015ab4d8_2017%20Play%20and%20Playthings%20Report%20EXCERPT.PDF)
* Playgrounds and Playground Design
	* [http://architekturfuerkinder.ch/](http://architekturfuerkinder.ch/) (!)
	* https://monstrum.dk/en/approach
	* [https://www.pgpedia.com/](https://www.pgpedia.com/)
* Sex as Play
	* https://www.thecatholicthing.org/2018/08/21/sex-as-play/
	* [Group Sex as Play: Rules and Transgression in Shared Non-monogamy](https://journals.sagepub.com/doi/abs/10.1177/1555412016659835)
	* [Nelson N. Foote: Sex as Play (1954)](https://academic.oup.com/socpro/article-abstract/1/4/159/1675357?redirectedFrom=fulltext)