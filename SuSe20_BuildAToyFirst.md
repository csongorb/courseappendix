# Build a toy (first) - Further Reading

Summer Semester 2020  
[Game Design // UE](http://gamedesign.ue-germany.de/)  
Prof. Csongor Baranyai

## Module Essentials

* https://bitbucket.org/account/user/btkgamedesign/projects/SUSE20_BUILDATOYFIRST
* https://itch.io/jam/suse20-build-a-toy-first
* Miro/Uhrwerk Orange/SuSe20 - Build a toy (first)
* Teams/SuSe20 - Build a toy (first)/Appendix/Files (!)
* Repository Structure
	* https://bitbucket.org/btkgamedesign/creditsthegame
	* https://keepachangelog.com/
	* https://www.gamesasresearch.com/

## Project Development

### Design Notes / Journals

* https://www.gamesasresearch.com/
* Jordan Mechner Journals
	* https://jordanmechner.com/store/the-making-of-karateka/
	* https://jordanmechner.com/store/the-making-of-prince-of-persia/
	* https://jordanmechner.com/backstage/journals/
* https://github.com/oliverbenns/john-carmack-plan

## Theory

* Experimentality
	* https://www.aplayfulpath.com/experimentality/
	* https://www.instituteofplay.org/
* Searching the Game
	* [Exploring Sim City: A Conscious Process of Discovery (Dan Moskowitz)](https://www.youtube.com/watch?v=eZfj7LEFT98)
	* [How Jonathan Blow Designs a Puzzle (Game Maker's Toolkit)](https://www.youtube.com/watch?v=2zK8ItePe3Y)
* Build the toy first
	* [Kyle Gabler: 2009 Global Game Jam Keynote (starts at 5:40)](https://www.youtube.com/watch?v=aW6vgW8wc6c)
		* https://www.gamasutra.com/view/feature/130848/how_to_prototype_a_game_in_under_7_.php
* Play
	* http://www.deepfun.com/articles/
	* https://en.wikipedia.org/wiki/Play_(activity)
	* [Documentary: The Promise of Play(YouTube)](https://www.youtube.com/playlist?list=PLA66825B802731F20)
* Bernie DeKoven
	* https://www.deepfun.com/bernie-dekoven-fun-theorist/
	* https://www.deepfun.com/
	* https://www.aplayfulpath.com
	* https://www.psychologytoday.com/gb/blog/having-fun
* Toys and Toy Design
	* https://www.wired.com/2011/01/the-5-best-toys-of-all-time/
	* https://museen.nuernberg.de/fileadmin/mdsn/pdf/Spielzeugmuseum/Downloads/Literaturliste_Spielzeug.pdf
	* Smarty Pants: Play & Playthings Report
		* [Play & Playthings Report 2018 - Excerpt](https://assets.website-files.com/5435eb4d1e426bb420ac990f/5c0e9721b6a5e9175bd940be_2018%20Play%20%26%20Playthings%20Report%20from%20Smarty%20Pants%20EXCERPT.pdf)
		* [Play & Playthings Report 2017 - Excerpt](https://uploads-ssl.webflow.com/5435eb4d1e426bb420ac990f/5a3170d30672a700015ab4d8_2017%20Play%20and%20Playthings%20Report%20EXCERPT.PDF)
* Playgrounds and Playground Design
	* http://architekturfuerkinder.ch/ (!)
	* https://monstrum.dk/en/approach
* Game Development Process
	* https://en.wikipedia.org/wiki/Cone_of_Uncertainty
* Games as Experiments (what if?)
	* http://www.experimental-gameplay.org/
	* https://what-if.xkcd.com/archive/
	* http://gamephilosophy.org/pcg2016/site/assets/files/1015/moring_-_experimental_systems.pdf
* Tools
	* http://leveldesignporn.csongorb.com/tooldesign.html
	* http://leveldesignporn.csongorb.com/anyleveleditor.html
* Play vs. Work
	* [Eron Rauch: Workification](https://videogametourism.at/tags/workification)
* Play and Playground Encyclopedia</div>
	* https://www.pgpedia.com/
	* https://tocaboca.com/magazine/
	* http://www.journalofplay.org/
* Sex as Play
	* https://www.thecatholicthing.org/2018/08/21/sex-as-play/
	* https://journals.sagepub.com/doi/abs/10.1177/1555412016659835
	* https://academic.oup.com/socpro/article-abstract/1/4/159/1675357?redirectedFrom=fulltext
