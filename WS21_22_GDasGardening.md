# Game Design as Gardening - Further Reading

Winter Semester 2021 / 2022  
[Game Design // UE](http://gamedesign.ue-germany.de/)  
[Prof. Csongor Baranyai](http://www.csongorb.com)  

## General

### Module Essentials

* Miro > Uhrwerk Orange > WS21/22 - GD as Gardening
* Teams > WS21/22 - GD as Gardening > Appendix > Files (!)

### Everything Devolution

* [http://devolution.online/](http://devolution.online/)
* [http://devolution.online/gdasgardeningsummit/](http://devolution.online/gdasgardeningsummit/)
* https://twitter.com/devolutionbln
* [Csongor Baranyai: Planting Games](https://medium.com/@csongorb/planting-games-9fc59c3f992f)

### Visible Development Histories

* http://www.museumofplay.org/collections/archival-video-game
* http://www.jordanmechner.com/backstage/journals/
* https://archive.org/details/infocomcabinet
* [https://www.unseen64.net/](https://www.unseen64.net/)
* https://bossfightbooks.com/products/spelunky-by-derek-yu
* DoubleFineProd: Devs Play (Youtube)
	* [Season 1](https://www.youtube.com/watch?v=hPnl60dZnbY&list=PLIhLvue17Sd6u2akeZZdYVBxNtfWZPm5W)
	* [Season 2](https://www.youtube.com/watch?v=HrPBCh-xDsQ&list=PLIhLvue17Sd5E75N5z8hK0-X7LXgGsJA5)
	* [Tim Schaefer Plays His Classics](https://www.youtube.com/watch?v=d3IidGmVLo4&list=PLIhLvue17Sd7jTtxS1rO_u2_6o64ZaGTO)
* https://www.polygon.com/2017/10/3/16408922/game-development-level-design (#blocktober)
* [https://thespelunkyshowlike.libsyn.com/](https://thespelunkyshowlike.libsyn.com/)
* Early Access / Patreon / Refinery / etc.

## Related Topics

### Always Beta / Open Development

* http://www.pcgamer.com/we-asked-developers-how-they-would-fix-early-access/
* https://www.polygon.com/2017/12/7/16745560/game-of-the-year-2017-pubg-warframe
* [http://www.enviv.de/](http://www.enviv.de/)
* [https://opendevelopmentmethod.org/](https://opendevelopmentmethod.org/)
* https://www.gamasutra.com/view/news/213339/Vlambeers_Performative_Game_Development__the_way_of_the_future.php

### Exploration / Experimentation

* Jonathan Blow
	* https://www.gdcvault.com/play/1014982/Truth-in-Game
	* http://the-witness.net/news/2011/11/designing-to-reveal-the-nature-of-the-universe/
* http://gamephilosophy.org/pcg2016/site/assets/files/1015/moring_-_experimental_systems.pdf
* [https://www.gamesasresearch.com/](https://www.gamesasresearch.com/)
* Game Makers Toolkit
	* [How Jonathan Blow Designs a Puzzle](https://www.youtube.com/watch?v=2zK8ItePe3Y)
	* [The Games That Designed Themselves](https://www.youtube.com/watch?v=kMDe7_YwVKI)
* [https://lostgarden.home.blog/2007/02/19/rockets-cars-and-gardens-visualizing-waterfall-agile-and-stage-gate/](https://lostgarden.home.blog/2007/02/19/rockets-cars-and-gardens-visualizing-waterfall-agile-and-stage-gate/)

### Nature & Technology

* http://kk.org/books/what-technology-wants/
* Objectorientation as Foundation
	* [http://platformstudies.com/](http://platformstudies.com/)
	* http://dinodini.weebly.com/design-constraints-and-integrity
	* Seminar with Alan Kay on Object Oriented Programming
		* https://www.youtube.com/watch?v=QjJaFG63Hlo
* [https://natureofcode.com/](https://natureofcode.com/)

### Testing / Feedback

* https://www.gamasutra.com/view/news/219946/Video_Make_your_games_better_with_playtesting_the_Uncharted_way.php
* https://www.juneydijkstra.com/wp-content/uploads/2014/05/dijkstra-juney-experimentation-project-analyzing-games-players.pdf
* [Games User Research SIG (YouTube)](https://www.youtube.com/channel/UCJI0-SDgfwxfb_hSADU7UyQ?feature=3Dautonav)
* http://leveldesignporn.csongorb.com/analytics.html

### Changing the Rules (Child Play)

* [Bernard de Koven: The Well Played Game](https://mitpress.mit.edu/books/well-played-game)
	* Changing the Game (p. 39 ff)
* [https://www.aplayfulpath.com/easier-to-change-game-than-change-people-playing/](https://www.aplayfulpath.com/easier-to-change-game-than-change-people-playing/)
* [Bernard de Koven: What If…We Changed the Rules of the Game?](https://www.youtube.com/watch?v=fNVUIAWNtH8)

### Finishing

* Derek Yu (Spelunky)
	* http://makegames.tumblr.com/post/1136623767/finishing-a-game
	* https://www.derekyu.com/makegames/deathloops.html
* http://www.gamasutra.com/blogs/RosstinMurphy/20150224/237145/FINISH_YOUR_GAME.php
* https://curiouscat.me/tha_rami/post/22725533
* http://pixeldiskurs.de/2017/12/10/pixeldiskurs-podcast-73-spiele-als-werk-vs-spiele-als-service/
* http://ramiismail.com/2016/04/an-inline-response-to-wage-slaves/

### Game Feel / Juice / Art of Screenshake

* https://github.com/grapefrukt/juicy-breakout
* Jan Willem Nijman
	* The Art of Screenshake
		* https://www.youtube.com/watch?v=3DAJdEqssNZ-U
	* https://thespelunkyshowlike.libsyn.com/36-game-feel-as-procrastination-with-jan-willem-nijman
* Why Your Death Animation Sucks
	* https://www.youtube.com/watch?v=3DpmSAG51BybY
* https://gamedevelopment.tutsplus.com/tutorials/bringing-your-game-to-life-in-10-simple-steps--cms-23447

### Tools

* http://www.gamasutra.com/view/feature/131850/designing_design_tools.php
* http://leveldesignporn.csongorb.com/tooldesign.html
* http://leveldesignporn.csongorb.com/anyleveleditor.html

### Prototyping

* Prototyping vs. Iterating
	* https://prezi.com/t9i-n5jqil6e/prototypingiterating/
* http://www.gdcvault.com/play/1019694/From-Game-Jam-to-Full
* http://www.lostgarden.com/2007/02/rockets-cars-and-gardens-visualizing.html
* Prototypes
	* https://2dboy.com/2009/03/06/the-world-of-goo-wasnt-built-in-a-day-part-1-of-7/
	* http://number-none.com/blow/prototypes/
	* http://asherv.com/threes/threemails/
 * https://www.wooga.com/about/hitfilter/

### Logic vs. Content

* Logic vs. Content
	* Game vs. Level Design
	* Narrativ Design vs. Game Writing
	* Code vs. Art
* http://leveldesignporn.csongorb.com/gamevsleveldesign.html
* http://uber.typepad.com/birthofagame/2009/04/vertical-slice-vs-horizontal-layer.html
* [Psychonauts 2 - First Playable (YouTube)](https://www.youtube.com/watch?v=3DOi3VJhB9rrA)
* [http://www.gameenginebook.com](http://www.gameenginebook.com)
* Modding
	* http://curious-expedition.wikia.com/wiki/Modding:Home
	* https://www.wikiwand.com/en/Mod_(video_gaming)
	* http://leveldesignporn.csongorb.com/modding.html

### Features / Software Architecture

* http://gdcvault.com/play/1020879/From-a-Cookie-to-the
* http://gamestudies.org/0802/articles/sicart
* [http://www.gameenginebook.com](http://www.gameenginebook.com)
* https://gameprogrammingpatterns.com/architecture-performance-and-games.html

### Metaphors

* [George Lakoff and Mark Johnson: Metaphors We Live](https://www.press.uchicago.edu/ucp/books/book/chicago/M/bo3637992.html)
* Hans Blumenberg: [Paradigmen zu einer Metaphorologie](https://www.suhrkamp.de/buch/hans-blumenberg-paradigmen-zu-einer-metaphorologie-t-9783518289013), etc.
* Visualisations
	* [https://gource.io/](https://gource.io/)
	* https://en.wikipedia.org/wiki/Unified_Modeling_Language

### Growth in Nature

* [https://en.wikipedia.org/wiki/Evolution](https://en.wikipedia.org/wiki/Evolution)
* [https://en.wikipedia.org/wiki/Genus](https://en.wikipedia.org/wiki/Genus) (Gattung)
* [Mutations | Genetics | Biology | FuseSchool (YouTube)](https://www.youtube.com/watch?v=lzhp5NuXo-k)
* [What is a coronavirus? - Elizabeth Cox (YouTube)](https://www.youtube.com/watch?v=D9tTi-CDjDU)
* [https://en.wikipedia.org/wiki/Permaculture](https://en.wikipedia.org/wiki/Permaculture)

### Vision

* http://www.gamasutra.com/view/news/281853/The_realities_of_being_a_game_designer_in_a_big_studio_like_BioWare.php
* http://www.gamasutra.com/view/feature/131617/collaborating_in_game_design.php
* [Zak McClendon: Welcome to the Yard Sale: A Practical Framework for Holistic Design Iteration (YouTube)](https://www.youtube.com/watch?v=PkZoGDKy_L4&list=PLAIAHfJvz15m0xE1OU_PUCEc3d1x5m7iq&index=8)
* [https://venturebeat.com/2018/07/08/mark-cerny-and-amy-hennig-a-fireside-chat-with-master-game-makers/view-all/](https://venturebeat.com/2018/07/08/mark-cerny-and-amy-hennig-a-fireside-chat-with-master-game-makers/view-all/)

### Oeuvre / Authorship

* http://narrativedesignporn.csongorb.com/authorship.html
* Examples
	* https://en.wikipedia.org/wiki/Edmund_McMillen
	* https://en.wikipedia.org/wiki/Jason_Rohrer
	* [http://vectorpoem.com/](http://vectorpoem.com/)
	* [https://www.increpare.com/](https://www.increpare.com/)
	* https://en.wikipedia.org/wiki/Hideo_Kojima
	* https://en.wikipedia.org/wiki/Woody_Allen
	* https://en.wikipedia.org/wiki/Quentin_Tarantino

### Game Preservation

* Game Preservation Society
	* https://www.gamepres.org/en/
	* https://www.youtube.com/channel/UC8D4j4oqi-xGwVwEBPZCn4w
* https://guides.lib.umich.edu/c.php?g=282989&p=5955093
* https://www.gamasutra.com/view/feature/134641/where_games_go_to_sleep_the_game_.php
* https://www.gamasutra.com/view/feature/135112/selecting_save_on_the_games_we_.php
* http://web.stanford.edu/group/htgg/cgi-bin/drupal/?q=3Dnode/1211
* [https://gamehistory.org/](https://gamehistory.org/)
* https://kotaku.com/why-some-video-games-are-in-danger-of-disappearing-fore-1789609791

## About Video Essays

### Examples

* about games
	* [Errant Signal (YouTube)](https://www.youtube.com/channel/UCm4JnxTxtvItQecKUc4zRhQ)
	* [Extra Credits (YouTube)](https://www.youtube.com/channel/UCCODtTcd5M1JavPCOr_Uydg)
	* [Game Makers Toolkit (YouTube)](https://www.youtube.com/channel/UCqJ-Xo29CKyLTjn6z2XwYAw)
* other
	* [The Coding Train (YouTube)](https://www.youtube.com/channel/UCvjgXvBlbQiydffZU7m1_aw)

### Software

* [https://obsproject.com/](https://obsproject.com/)
* PowerPoint (Presentation Mode)
* Adobe Premiere Pro (Adobe CC)

## Quotes

### Bertolt Brecht: Garden in Progress

> […] Doch  
> Wie der Garten mit dem Plan  
> Wächst der Plan mit dem Garten.

### George Lakoff and Mark Johnson: Metaphors We Live By / Leben in Metaphern

> We have found, on the contrary, that metaphor is pervasive in everyday life, not just in language but in thought and action. Our ordinary conceptual system, in terms of which we both think and act, is fundamentally metaphorical in nature. (P. 3)

> Wir haben dagegen festgestellt, daß die Metapher unser Alltagsleben durchdringt, und zwar nicht nur unsere Sprache, sondern auch unser Denken und Handeln. Unser alltägliches Konzeptsystem, nach dem wir sowohl denken als auch handeln, ist im Kern und grundsätzlich metaphorisch.” (S.11)

---

> Our concepts structure what we perceive, how we get around in the world, and how we relate to other people. Our conceptual system thus plays a central role in defining our everyday realities. If we are right in suggesting that our conceptual system is largely metaphorical, then the way we think, what we experience, and what we do every day is very much a matter of metaphor. (P. 3)

> Unsere Konzepte strukturieren das, was wir wahrnehmen, wie wir uns in der Welt bewegen und wie wir uns auf andere Menschen beziehen. Folglich spielt unser Konzeptsystem bei der Definition unserer Alltagsrealitäten eine zentrale Rolle. Wenn, wie wir annehmen, unser Konzeptsystem zum größten Teil metaphorisch angelegt ist, dann ist unsere Art zu denken, unser Erleben und unser Alltagshandeln weitgehend eine Sache der Metapher.” (S. 11)

---

> The expense of metaphor is understanding and experiencing one kind of thing in terms of another. (P. 5)

> Das Wesen der Metapher besteht darin, daß wir durch sie eine Sache oder einen Vorgang in Begriffen einer anderen Sache bzw. eines anderen Vorgangs verstehen und erfahren können. (S. 13)

---

> We would now like to turn to metaphors that are outside our conventional conceptual system, metaphors that are imaginative and creative. Such metaphors are capable of giving us a new understanding of our experience. Thus, they can give new meaning to our pasts, to our daily activity, and to what we know and believe. (P. 139)

> Wir wenden uns nun solchen Metaphern zu, die außerhalb unseres konventionalisierten Konzeptsystems liegen; dabei handelt es sich um solche Metaphern, die der individuellen Phantasie und Kreativität entspringen. Metaphern dieser Art können dazu beitragen, daß wir unsere Erfahrung in einem neuen Licht sehen. Folglich können sie unsere Vergangenheit, unseren täglichen Aktivitäten und unseren Wissens- und Glaubenssystem eine neue Bedeutung geben. (S. 161)

---

> Many of our activities (arguing, solving problems, budgeting time, etc.) are metaphorical in nature. The metaphorical concepts that characterise those activities structure our present reality. New metaphors have the power to create a new reality. This can begin to happen when we start to comprehend our experience in terms of a metaphor, and it becomes a deeper reality when we begin to act in terms of it. If a new metaphor enters the conceptual system that we base our actions on, it will alter that conceptual system and the perceptions and actions that the system gives rise too. Much of cultural change arises from the introduction of new metaphorical concepts and the loss of old ones. (P. 145)

> Viele unserer Aktivitäten (argumentieren, Probleme lösen, mit der Zeit haushalten usw.) sind ihrem Wesen nach metaphorisch. Die metaphorischen Konzepte, die für diese Aktivitäten charakteristisch sind, strukturieren unsere gegenwärtige Realität. Neue Metaphern haben die Kraft, neue Realität zu schaffen. Dieser Prozeß kann an dem Punkt beginnen, an dem wir anfangen, unsere Erfahrung von einer Metapher her zu begreifen, und er greift tiefer in unsere Realität ein, sobald wir von einer Metapher her zu handeln beginnen. Wenn wir eine neue Metapher in das Konzeptsystem aufnehmen, das unsere Handlungen strukturiert, dann verändern sich dadurch das Konzeptsystem wie auch die Wahrnehmungen und Handlungen, die dieses System hervorbringt. Kultureller Wandel entsteht häufig dadurch, daß neue metaphorische Konzepte eingeführt werden und alte verschwinden. (S. 168)

### Developers Using the Metaphor

> Games develop in a way that I can only describe as organic, in that they tend to start as a core and then grow outward.

[David OReilly](https://www.davidoreilly.com/) in [Chris Kerr: Animation vs. game dev](https://www.gamasutra.com/view/news/296771/Animation_vs_game_dev_Everything_creator_David_OReilly_breaks_down_the_differences.php)

> Imagine that we are gardeners. We seed a diverse portfolio of projects, some high risk, so low. As we add team members and customer feedback, the projects begin to flourish, winding their way up towards the sunlight of launch. The core team development practices can be agile since we want to encourage short cycles of rapid feedback. The best ones start to blossom and ripen with obvious customer value. However, some projects are sickly and seem unlikely to produce much of anything.  
> As gardeners, it is also our job to groom the garden. The weak projects are trimmed back and turned into mulch that helps nourish the most successful projects as well as create a bed for seeding even more promising ideas. The best projects are harvested and sent off to customers, rich with ripe features, bursting with value. We are constantly seeding new projects to keep our portfolio balanced.  
> [...]  
> Returning to the garden metaphor, as you nurture these areas of value, you should always have projects that are ripe for release. Publishing can be as simple as creating a public release that turns off all the experimental projects, leaving only your best features to show the public. I think of this as harvesting a ripe crop for your favorite customers.
> As time passes, the little ideas that you seeded earlier will mature and produce a new crop of high value features. The process just keeps repeating, season after season, release after release.

[Daniel Cook: Rockets, Cars and Gardens](https://lostgarden.home.blog/2007/02/19/rockets-cars-and-gardens-visualizing-waterfall-agile-and-stage-gate/)

> The process of designing the gameplay of [Braid], was more like discovering things that already exist, than [...] creating something new and arbitrary.

[Jonathan Blow: Truth in Game Design](https://www.gdcvault.com/play/1014982/Truth-in-Game)

### OOP Origins

> my particular motivation when I started thinking about objects was simply that of biological cells. and one of the things that is difficult to escape when you look at biological cells is first the extent to which the outside world is kept away from the cell membrane and then the other thing is is that the cells aren't completely in isolation. they communicate by means of sending messages to each other.

[Alan Kay on Object Oriented Programming](https://www.youtube.com/watch?v=QjJaFG63Hlo)

> We were designing a language for simulation modelling, and such models are most easily conceived of in terms of cooperating objects. Our approach, however, was general enough to be applicable to many aspects of system development.

Ole-Johan Dahl: The Birth of Object Orientation: the Simula Languages

### David Holmgren: Permaculture

> In every aspect of nature, from the internal workings of organisms to whole ecosystems, we find the connections between things are as important as the things themselves. Thus the purpose of a functional and self-regulating design is to place elements in such a way that each serves the needs and accepts the products of other elements.

> In jedem Aspekt der Natur, von Vorgängen im Inneren einzelner Organismen bis hin zu ganzen Ökosystemen, können wir erkennen, dss die Verbindung zwischen den Dingen ebenso wichtig sind wie die Dinge selbst. Somit ist der Zweck einer funktionalen und selbstregulierenden Gestaltung, die Gestaltungelemente so anzuordnen, dass ein jedes die Bedürfnisse der jeweils anderen Elemente erfüllt und deren Produkte anerkennt und würdigt. (S. 219)

---

> Computers are the most obvious feature of the information economy, but changes in the way we think, especially the emergence of design thinking, are more fundamental to the information economy than the hardware and software we use. Permaculture itself is part of this thinking revolution.

> Obwohl Computer das augenfälligste Merkmal der Informationsökonomie sind, sind die Veränderungen unseres Denkens, insbesondere das Auftreten des Design-Thinking, viel grundlegender für die Informationsökonomie als die Hard- und Software, die wir dabei verwenden. Die Permakultur selbst ist Teil dieser Revolution des Denkens.

---

> As a consultant working with new rural land owners, an important part of my job is helping decode the signs in the landscape of past events, use and potentials which are not readily visible. Once we start to see all landscapes in this way, we understand that, although chance events are the cause of much of what we see, the patterns described by ecologists from study of natural landscapes are also useful in understanding landscapes radically changed by human intervention.

---

> Observation, the first principle of permaculture, is the key to making use of succession. Landscapes embody dynamic processes with a history and, to some extent, a future that can be read from the signs observable now. This ability is a critical observational skill that can be developed.7 Dynamic change in nature and landscape is easily accepted as an intellectual concept, but our direct visual experience of landscape is most commonly a static image or picture in which the past and future are invisible.

> Als erstes Permakulturprinzip ist die Beobachtung der Schlüssel, um die Dynamik der Sukzession nutzbar zu machen. Landschaften verkörpern dynamische Prozesse mit einer Geschichte und einer Zukunft, die zumindest zu einem gewissen Grad aus den in der Gegenwart beobachtbaren Zeichen abgelesen werden können. Dieser Fähigkeit ist ein entscheidender Aspekt der Beobachtungsgabe, die wir herausbilden können. Auf der Verstandesebene lässt sich dynamischer Wandel in Natur und Landschaft leicht akzeptieren, aber unser unmittelbares visuelles Erleben von Landschaft ist in den meisten Fällen mit einem statischen Bild oder Konzept verbunden, in dem weder Vergangenheit noch Zukunft vorkommen. (S. 336)

---

> Observation and interaction involve a two-way process between subject and object: the designer and the system.

---

> ...when working with complex natural systems we need to remember that we don’t understand, let alone control, all the factors; and that cause and effect are often a loop or a web, rather than a linear chain.

---

> In attempting to fix any system, we may damage another that is working perfectly well.

---

> Even in the garden, much of the work we do is not planting, but removing or cutting back unwanted growth.

---

> Fundamentally, permaculture design principles arise from a way of perceiving the world which is often described as ‘systems thinking’ and ‘design thinking’ (see Principle 1 - The Thinking and Design Revolution). Other examples of systems and design thinking include:

>* the Whole Earth Review, and its better-known offshoot the Whole Earth Catalogue, edited by Stewart Brand, did much to publicise systems and design thinking as a central tool in the cultural revolution to which permaculture is a contribution

---

> the academic discipline of cybernetics, systems thinking has been an esoteric and difficult subject, closely associated with the emergence of computing and communication networks and many other technological applications

> Die akademische Disziplin der Kybernetik ist eine ausgefeilte und hochspezialisierte Form des Systemdenkens, das in enger Verbindung mit dem Aufkommen von Informationstechnik und Kommunikationsnetzwerken stand. (S. 33)

---

> Complex systems that work tend to evolve from simple ones that work, so finding the appropriate pattern for that design is more important than understanding all the details of the elements in the system.

> Komplexe funktionierende Systeme entwickeln sich in der Regel aus einfachen funktionierenden Systemen heraus - es ist also wichtiger, geignete Strukturen für die Gestaltung zu finden, als alle Details der Systemelemente zu verstehen. (S. 186)

---

> As designers and developers of gardens and other productive systems, we can see our role as analogous to the nursing mother in a number of ways.

> * In the early establishment phase, gardens are totally dependent on our care and attention. For example, newly planted trees must be protected from browsing animals and may require weed control, water and fertiliser to get started.
> * The designer/manager who appears to be all-powerful, like the nursing mother, acts without being able to control or even understand all the factors that might impact on the garden.
> * If the design is effective, the garden becomes progressively more self-reliant and less dependent on our care, although like a young child it may require intervention at times to save it from external and internal danger.
> * If our design and care are truly inspired, the garden, as well as becoming more robust, will develop a degree of self-regulation and balance analogous to children growing through adolescence to be responsible adults.

---

> Most people tend to design and establish gardens (unconsciously following the initial floristic composition model) and are disappointed that there isn’t enough space for everything and that some things die out or grow poorly as the system changes.

> * Sometimes the response to understanding that some trees grow very large is to plant them at wide spacings in shelterbelts, where they struggle with competing grass and damaging wind (which actually increases in speed as it whistles between the slowly growing trees).
> * In other situations, people give up growing some species because frost, wind and sun damage the plant, when a more mature system with more shade and shelter allows these delicate species to thrive.

### Bill Mollison: Permaculture - Designers Manual

> Design as a selection of options or pathways based on decisions  
> Any sensible design gives a place to start. The evolution of the design is a matter for trial, following observation, and then acting on that information.  
> I sometimes think that the only real purpose of an initial design is to evolve some sort of plan to get one started in an otherwise confusing and complex situation. If so, a design has a value for this reason alone, for as soon as we decide to start doing, we learn how to proceed.  
> [...]  
> All of this can be plotted, rather like the decision pattern a tree makes as it branches upwards. Some options are impractical, or in conflict with other decisions and ethics, and are therefore unavailable. (See Figure 3.6).  
> [...]  
> Options open up of close down on readily available evidence or as decision-points are reached. All will affect the number and direction of future actions, hence the overall design.  

### Other

> A complex system that works is invariably found to have evolved from a simple system that works. […] A complex system designed from scratch never works and cannot be made to work.

John Gall: Systemantics: How Systems Work and Especially How They Fail

> We believe it is time for those of us in the humanities to serously consider the lowest level of computing systems and to understand how these systems relate to culture and creativity.  
> [...] to promote the investigation of underlying computing systems and how they enable, constrain, shape, and support the creative work that is done on them.

Nick Montfort and Ian Bogost: Racing the Beam

### Michael Pollan: Second Nature

> The green thumb is the gardener who can nimbly walk the line between the dangers of over- and undercultivation, between pushing nature too far and giving her too much ground. [...] To occupy such a middle ground is not easy - the temptation is always to either take complete control or relinquish it altogether, to invoke your own considerable (but in the end overrated) power or to bend to nature's. (P. 124)

> Wir wollen den Gärtner einen Grünen Daumen nennen, der geschickt einen Weg findet zw. den Gefahren der Über- und der Unterkultivierung, zw. dem übermässigen Bedrängen der Natur und dem zu starken Nachgeben ihr gegenüber. […] Sich auf einen solchen Mittelweg zu bewegen, ist nicht einfach - immer ist da die Versuchung, entweder die Kontrolle vollständig zu übernehmen oder alles sich selbst zu überlassen. (S. 181)

---

> Der Grüne Daumen weiss, es zieht in seiner Garten nicht die Fäden. Und was ebenso wichtig ist: Das ist ihn auch lieber so. Er hat sogar den Verdacht, dass ein Garten, den er vollkommen unter seiner Kontrolle hätte, ein farbloser Ort wäre, inhaltsleer und uninteressant. […] Den Garten gut zu bestellen heisst, mitten im Gemurmel der realen Welt glücklich und zufrieden zu sein, sich nicht aus der Ruhe bringen zu lassen, wenn diese Welt in ihrer unbezähmbaren Üppigkeit sich weigert, sich unseren Vorstellungen entsprechend zurechtstutzen zu lassen. (S. 190)

### Kevin Kelly: What Technology Wants

> The technium is now as great a force in our world as nature, and our response to the technium should be similar to our response to nature. We can’t demand that technology obey us any more than we can demand that life obey us. Sometimes we should surrender to its lead and bask in its abundance, and sometimes we should try to bend its natural course to meet our own. We don’t have to do everything that the technium demands, but we can learn to work with this force rather than against it.

> And to do that successfully, we first need to understand technology’s behavior. In order to decide how to respond to technology, we have to figure out what technology wants.

---

> At first, this notion of technological independence is very hard to grasp. We are taught to think of technology first as a pile of hardware and secondly as inert stuff that is wholly dependent on us humans. In this view, technology is only what we make. Without us, it ceases to be. It does only what we want. And that’s what I believed, too, when I set out on this quest. But the more I looked at the whole system of technological invention, the more powerful and self-generating I realized it was.

---

> Evolution is driven toward certain recurring and inevitable forms by two pressures: 1. The negative constraints cast by the laws of geometry and physics, which limit the scope of life’s possibilities. 2. The positive constraints produced by the self-organizing complexity of interlinked genes and metabolic pathways, which generate a few repeating new possibilities.

> These two dynamics create a push in evolution that gives it a direction. Both of these two dynamics continue to operate in the technium as well and shape the inevitabilities along the course of the technium.

---

> These combinations are like mating. They produce a hereditary tree of ancestral technologies. Just as in Darwinian evolution, tiny improvements are rewarded with more copies, so that innovations spread steadily through the population. Older ideas merge and hatch idea-lings. Not only do technologies form ecosystems of cross-supported allies, but they also form evolutionary lines. The technium can really only be understood as a type of evolutionary life.

---

> But self-duplication is a radical new force in the technium. The mechanical ability to make perfect copies of oneself and then occasionally create an improvement before copying, unleashes a type of independence that is not easily controlled by humans. Endless, ever-quickening cycles of reproduction, mutation, and bootstrapping can send a technological system into

---

> The trends cataloged here are 13 facets of this urge. This list is not meant to be comprehensive. Other people may draw a different profile. I would also expect that as the technium expands in coming centuries and our understanding of the universe deepens, we will add more facets to this exotropic push.

---

> The complexity of a system rises in a series of steps, where each higher level congeals into a new wholeness.

---

> The sequence of the technium is akin to the development of an organism as it grows through a scripted series of stages.

---

> We can forecast the future of almost any invention working today by imagining it evolving into dozens of narrow uses. Technology is born in generality and grows to specificity

---

> The global internet’s nearly organic interdependence and emerging sentience make it wild, and its wildness draws our affections. We are deeply attracted to its beauty, and its beauty resides in its evolution.

---

> The seed “wants” to be a plant. More precisely, the poppy seed is designed to grow stems, leaves, and flowers of a precise type.

---

> With minor differences, the evolution of the technium — the organism of ideas — mimics the evolution of genetic organisms. The two share many traits: The evolution of both systems moves from the simple to the complex, from the general to the specific, from uniformity to diversity, from individualism to mutualism, from energy waste to efficiency, and from slow change to greater evolvability. The way that a species of technology changes over time fits a pattern similar to a genealogical tree of species evolution. But instead of expressing the work of genes, technology expresses ideas.

---

> As technology invariably does, one invention prepares the ground for the next, and every corner of the technium evolves in a seemingly predetermined sequence.

---

> But we have been slow to learn that systems—all systems—generate their own momentum.

---

> The technium wants what we design it to want and what we try to direct it to do. But in addition to those drives, the technium has its own wants. It wants to sort itself out, to self-assemble into hierarchical levels,

---

> Humans are both master and slave to the technium, and our fate is to remain in this uncomfortable dual role.

---

> The global internet's nearly organic interdependence and emerging sentience make it wild, and it's wildness draws our affections. We are deeply attracted to its beauty, and its beauty resides in its evolution.

---

> The evolution of science and technology parallels the evolution of nature. The major technological transitions are also passages from one level of organization to another.

---

> By following what technology wants, we can be more ready to capture its full gifts.

---

> In a dim way it chooses to satisfy its wants by heading one way and not another.

> With the technium, want does not mean thoughtful decisions. I don’t  believe the technium is conscious (at this point). Its mechanical wants are not carefully considered deliberations but rather tendencies. Leanings. Urges. Trajectories. The wants of technology are closer to needs, a compulsion toward something. Just like the unconscious drift of a sea cucumber as it seeks a mate. The millions of amplifying relationships and countless circuits of influence among parts push the whole technium in certain unconscious directions.
