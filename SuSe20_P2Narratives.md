# P2 / Narratives - Further Reading

Summer Semester 2020  
[Game Design // UE](http://gamedesign.ue-germany.de/)  
Prof. Csongor Baranyai

## Module Essentials

* https://bitbucket.org/account/user/btkgamedesign/projects/SUSE20_P2_CSONGOR
* https://itch.io/jam/suse20-p2-narratives-csongor
* Miro/Uhrwerk Orange/SuSe20 - P2 Narratives
* Teams/SuSe20 - P2 Narratives/Appendix/Files (!)
* https://bitbucket.org/btkgamedesign/creditsthegame
* Resources
	* http://narrativedesignporn.csongorb.com/
	* https://prezi.com/9dpozjsovdjp/narrative-design/
	* https://prezi.com/9t5m2-xhcrdk/gamessystems/
	* https://www.goodreads.com/review/list/40472585-csongor-baranyai?shelf=interactive-storytelling
	* https://bitbucket.org/csongorb/templateunitywalkingsim (Fork me on Bitbucket)
	* https://wiki.gd-ue.de/doku.php?id=guidelines:project_dev_structure

## Background

### Public Domain

* Collections / General
	* http://publicdomainreview.org/guide-to-finding-interesting-public-domain-works-online/
	* http://www.vulture.com/2017/09/30-hollywood-classics-streaming-free-in-the-public-domain.html
	* http://publicdomainreview.org/
	* http://www.feedbooks.com/publicdomain?locale=en
* Ideas
	* Robinson Crusoe
	* Shakespeare
	* Fairy Tales
	* Sherlock Holmes
	* Tom Sawyer / Huckleberry Finn
	* Dracula
	* Jule Verne
	* Winnetou
	* Frankenstein
	* Edgan Allan Poe
	* Die Leiden des jungen Werthers

## Project Development

### Design Notes / Journals

* https://www.gamesasresearch.com/
* Jordan Mechner Journals
	* https://jordanmechner.com/store/the-making-of-karateka/
	* https://jordanmechner.com/store/the-making-of-prince-of-persia/
	* https://jordanmechner.com/backstage/journals/
* https://github.com/oliverbenns/john-carmack-plan

### Tools (for Prototyping)

* https://bitbucket.org/csongorb/templateunitywalkingsim
* https://bitbucket.org/csongorb/workshopnarrativedesign
	* https://twinery.org/
	* Foundobject / Analogue
	* Gamebook / Fabled Lands
	* http://inform7.com/
	* https://www.renpy.org/
* http://www.rpgmakerweb.com/products/programs/rpg-maker-mv
* https://www.inklestudios.com/ink/
* https://ledoux.itch.io/bitsy
* https://monogatari.io/documentation/
* https://www.prezi.com

## Video Essays

### Examples

* about games
	* [Errant Signal (YouTube)](https://www.youtube.com/channel/UCm4JnxTxtvItQecKUc4zRhQ)
	* [Extra Credits (YouTube)](https://www.youtube.com/channel/UCCODtTcd5M1JavPCOr_Uydg)
	* [Game Makers Toolkit (YouTube)](https://www.youtube.com/channel/UCqJ-Xo29CKyLTjn6z2XwYAw)
* other
	* [The Coding Train (YouTube)](https://www.youtube.com/channel/UCvjgXvBlbQiydffZU7m1_aw)

### Software

* https://obsproject.com/
* PowerPoint (Presentation Mode)
* Adobe Premiere Pro (Adobe CC)

## Theory

### On Walking Simulators

* [What Makes a Good Detective Game? (Game Maker's Toolkit)](https://www.youtube.com/watch?v=gwV_mA2cv_0&t!s)
	* https://www.youtube.com/watch?v=gwV_mA2cv_0&t!s
* [Christian Huberts: Missverständnis »Walking Simulator«](https://www.youtube.com/watch?v=3vnwHJ3IWY8)
	* https://www.slideshare.net/games_rv/missverstandnis-walkingsimulator-christian-huberts
* https://en.wikipedia.org/wiki/Fl%C3%A2neur
* https://medium.com/@csongorb/der-transmedia-storyteller-als-krimiautor-a73742aa188

### Walking Simulator Examples

* http://gonehomegame.com/
* https://fullbright.company/2013/02/15/gone-homes-input-system/
* [BioShock 2: Minerva's Den with designer/writer Steve Gaynor](https://www.twitch.tv/idlethumbs/video/38596011?filter=archives&sort=time)
* https://blog.thimbleweedpark.com/
* https://kotaku.com/rise-of-the-tomb-raiders-new-blood-ties-expansion-is-a-1787616902
* http://megapanda.net/14-uncharted-4/807-epilogue
* http://twistedtreegames.com/proteus/
* [Last Of Us: Intro](https://www.youtube.com/watch?v=9Gk-QXZkUL8)
* http://dear-esther.com/
* http://www.thinkwithportals.com/
* http://en.wikigta.org/wiki/Body_bags_%28GTA_San_Andreas%29
* http://www.herstorygame.com/

### Manifestos

* https://itch.io/jam/manifesto-jam
* https://www.blog.radiator.debacle.us/2017/04/a-survey-of-video-game-manifestos.html
